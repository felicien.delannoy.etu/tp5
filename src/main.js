import Router from './Router';
import PizzaList from './pages/PizzaList';
import PizzaForm from './pages/PizzaForm';
import Component from './components/Component';

const pizzaList = new PizzaList([]),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.menuElement = document.querySelector('.mainMenu');
Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

document.querySelector(
	'.logo'
).innerHTML += `<small>les pizzas c'est la vie</small>`;

// détection des boutons précédent/suivant du navigateur :
// on lit l'url courante dans la barre d'adresse et on l'envoie au Router
window.onpopstate = () => Router.navigate(document.location.pathname, false);
// affichage de la page initiale :
// même traitement que lors de l'appui sur les boutons précédent/suivant
window.onpopstate();

// B.2. Charger un fichier statique
function displayNews(html) {
	const newsContainer = document.querySelector('.newsContainer');
	// injection du contenu chargé dans la page
	newsContainer.innerHTML = html;
	// affichage du bandeau de news
	newsContainer.style.display = '';
	// gestion du bouton fermer
	const closeButton = newsContainer.querySelector('.closeButton');
	closeButton.addEventListener('click', event => {
		event.preventDefault();
		newsContainer.style.display = 'none';
	});
}
fetch('./news.html')
	.then(response => response.text())
	.then(displayNews);
